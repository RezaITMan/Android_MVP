package ir.wandp.View;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import ir.wandp.R;

public class ImageViewFragment extends Fragment {

    public static ImageViewFragment newInstance(String url) {
        Bundle arguments = new Bundle();
        arguments.putString("url",url);
        ImageViewFragment fragment = new ImageViewFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_image_preview, container, false);
        ImageView imageView=mainView.findViewById(R.id.img_image_preview);
        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        Glide.with(getContext()).load(getArguments().getString("url")).into(imageView);
        return mainView;
    }
}
