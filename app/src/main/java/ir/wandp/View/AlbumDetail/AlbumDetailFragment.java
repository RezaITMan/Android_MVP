package ir.wandp.View.AlbumDetail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import ir.wandp.Presenter.AlbumDetail.AlbumDetailPresenter;
import ir.wandp.Presenter.AlbumDetail.AlbumDetailPresenterImp;
import ir.wandp.R;
import ir.wandp.Service.AlbumDetail.AlbumDetailInteractorImp;
import ir.wandp.Service.Model.AlbumDetail;

public class AlbumDetailFragment extends Fragment implements AlbumDetailView {

    RecyclerView albumList;
    AlbumDetailPresenter albumPresenter;
    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView mTextViewStatus;
    ProgressBar mProgressBar;
    long id;

    AlbumDetailAdapter mAlbumAdapter;

    public static AlbumDetailFragment newInstance(long id,String title) {
        Bundle arguments = new Bundle();
        arguments.putLong("id",id);
        arguments.putString("title",title);
        AlbumDetailFragment fragment = new AlbumDetailFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_album_detail, container, false);
        albumList = mainView.findViewById(R.id.rv_home);
        mSwipeRefreshLayout = mainView.findViewById(R.id.srl_album);
        mTextViewStatus = mainView.findViewById(R.id.tv_album_status);
        mProgressBar = mainView.findViewById(R.id.pb_album);

        id=getArguments().getLong("id");

        Toolbar toolbar = mainView.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        toolbar.setTitle(getArguments().getString("title",toolbar.getTitle().toString()));

        albumPresenter = new AlbumDetailPresenterImp(this, new AlbumDetailInteractorImp());

        mAlbumAdapter = new AlbumDetailAdapter();
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(),3);
        albumList.setLayoutManager(mLayoutManager);
        albumList.setAdapter(mAlbumAdapter);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initData();
            }
        });


        initData();
        return mainView;
    }

    private void initData() {
        albumPresenter.getAlbumDetail(id);
    }



    @Override
    public void showProgress() {
        mTextViewStatus.setText("");
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
        mTextViewStatus.setText("");
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void OnAlbumDetailReceived(ArrayList<AlbumDetail> albums) {
        mAlbumAdapter.setAlbums(albums);
    }

    @Override
    public void onErrorReceived(String error) {
        hideProgress();
        if(mAlbumAdapter.getItemCount()==0) {
            mTextViewStatus.setText(error);

        }
    }
}
