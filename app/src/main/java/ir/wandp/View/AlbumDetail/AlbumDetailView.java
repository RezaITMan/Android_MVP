package ir.wandp.View.AlbumDetail;

import java.util.ArrayList;

import ir.wandp.Service.Model.AlbumDetail;

public interface AlbumDetailView {
    void showProgress();

    void hideProgress();

    void OnAlbumDetailReceived(ArrayList<AlbumDetail> albums);

    void onErrorReceived(String error);

}
