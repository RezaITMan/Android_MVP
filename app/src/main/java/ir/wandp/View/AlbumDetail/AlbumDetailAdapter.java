package ir.wandp.View.AlbumDetail;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ir.wandp.R;
import ir.wandp.Service.Model.AlbumDetail;
import ir.wandp.Service.RequestManager;
import ir.wandp.View.FragmentNavigation;
import ir.wandp.View.ImageViewFragment;

public class AlbumDetailAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<AlbumDetail> details;

    public AlbumDetailAdapter(ArrayList<AlbumDetail> details) {
        this.details = details;
    }
    public AlbumDetailAdapter() {
        this.details = new ArrayList<>();
    }

    public void setAlbums(ArrayList<AlbumDetail> albums) {
        this.details = albums;
        notifyDataSetChanged();
    }

    public  class AlbumViewHolder extends RecyclerView.ViewHolder {
        public TextView mTVName;
        public ImageView mImageView;
        public AlbumViewHolder(View v) {
            super(v);
            mTVName = (TextView) v.findViewById(R.id.tv_detail_code);
            mImageView=v.findViewById(R.id.iv_detail);
        }

        public void bind(int position,final AlbumDetail detail) {

            mTVName.setText(detail.getCode());
            Glide.with(mImageView.getContext()).load(RequestManager.BaseUrl+detail.getThumbPath()).into(mImageView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentNavigation.getInstance().addFragment(ImageViewFragment.newInstance(RequestManager.BaseUrl+details.get(getAdapterPosition()).getThumbPath()));
                }
            });
        }


    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_album_detail, parent, false);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return  new AlbumViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        AlbumViewHolder albumViewHolder = (AlbumViewHolder) holder;
        albumViewHolder.bind(position,details.get(position));
    }

    @Override
    public int getItemCount() {
        return details.size();
    }
}
