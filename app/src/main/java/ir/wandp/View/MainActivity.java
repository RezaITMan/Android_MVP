package ir.wandp.View;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ir.wandp.R;
import ir.wandp.View.Album.AlbumFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentNavigation fragmentNavigation = FragmentNavigation.newInstance(this);
        if(savedInstanceState==null)
            fragmentNavigation.replaceFragment(AlbumFragment.newInstance());

    }



}
