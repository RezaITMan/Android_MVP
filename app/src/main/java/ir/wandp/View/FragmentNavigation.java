package ir.wandp.View;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import ir.wandp.R;

public class FragmentNavigation {
    private AppCompatActivity activity;
    private static FragmentNavigation fragmentNavigation;
    FragmentManager fm;
    Toast toast;

    private FragmentNavigation(AppCompatActivity activity) {
        this.activity = activity;
        fm = activity.getSupportFragmentManager();
        toast = Toast.makeText(activity , "sure to exit" , Toast.LENGTH_SHORT);
    }

    public static FragmentNavigation newInstance(AppCompatActivity activity) {
        fragmentNavigation = new FragmentNavigation(activity);
        return fragmentNavigation;
    }

    public static FragmentNavigation getInstance() {
        if (fragmentNavigation == null) {
            throw new RuntimeException("FragmentNavigation has not initialize");
        }
        return fragmentNavigation;
    }

    public void addFragment(Fragment fragment) {
        fm.beginTransaction().add(R.id.container, fragment).addToBackStack("test").commit();
    }

    public void replaceFragment(Fragment fragment){
        fm.beginTransaction().replace(R.id.container, fragment).commit();
    }

    public void removeFragment() {
        if (fm.getFragments().size() == 1) {
            fm.popBackStack();
            toast.show();
            return;
        } else if (toast.getView().isShown()) {
            activity.finish();
        } else fm.popBackStack();
    }
}
