package ir.wandp.View.Album;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import ir.wandp.Presenter.Album.AlbumPresenter;
import ir.wandp.Presenter.Album.AlbumPresenterImp;
import ir.wandp.R;
import ir.wandp.Service.Album.AlbumInteractorImp;
import ir.wandp.Service.Model.Album;

public class AlbumFragment extends Fragment implements AlbumView {

    RecyclerView albumList;
    AlbumPresenter albumPresenter;
    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView mTextViewStatus;
    ProgressBar mProgressBar;

    static AlbumAdapter mAlbumAdapter;

    public static AlbumFragment newInstance() {
        Bundle arguments = new Bundle();
        AlbumFragment fragment = new AlbumFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_home, container, false);
        albumList = mainView.findViewById(R.id.rv_home);
        mSwipeRefreshLayout = mainView.findViewById(R.id.srl_album);
        mTextViewStatus = mainView.findViewById(R.id.tv_album_status);
        mProgressBar = mainView.findViewById(R.id.pb_album);

        Toolbar toolbar = mainView.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        albumPresenter = new AlbumPresenterImp(this, new AlbumInteractorImp());

        mAlbumAdapter = new AlbumAdapter();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        albumList.setLayoutManager(mLayoutManager);
        albumList.setAdapter(mAlbumAdapter);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initData();
            }
        });

        //setHasOptionsMenu(true);
        initData();
        return mainView;
    }

    private void initData() {
        albumPresenter.getAlbumList(null);
    }

   /* @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.main_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, menuInflater);
    }*/

    @Override
    public void showProgress() {
        mTextViewStatus.setText("");
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
        mTextViewStatus.setText("");
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void OnAlbumReceived(ArrayList<Album> albums) {
        mAlbumAdapter.setAlbums(albums);
    }

    @Override
    public void onErrorReceived(String error) {
        hideProgress();
        if(mAlbumAdapter.getItemCount()==0) {
            mTextViewStatus.setText(error);

        }
    }
}
