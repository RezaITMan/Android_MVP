package ir.wandp.View.Album;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ir.wandp.R;
import ir.wandp.Service.Model.Album;
import ir.wandp.View.AlbumDetail.AlbumDetailFragment;
import ir.wandp.View.FragmentNavigation;

public class AlbumAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Album> albums;

    public AlbumAdapter(ArrayList<Album> albums) {
        this.albums = albums;
    }
    public AlbumAdapter() {
        this.albums = new ArrayList<>();
    }

    public void setAlbums(ArrayList<Album> albums) {
        this.albums = albums;
        notifyDataSetChanged();
    }

    public  class AlbumViewHolder extends RecyclerView.ViewHolder {
        public TextView mTVName;
        public AlbumViewHolder(View v) {
            super(v);
            mTVName = (TextView) v.findViewById(R.id.tv_album_name);
        }

        public void bind(int position,final Album album) {
           itemView.setTag(position);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentNavigation.getInstance().addFragment(AlbumDetailFragment.newInstance(albums.get(getAdapterPosition()).getId(),albums.get(getAdapterPosition()).getName()));
                }
            });
            mTVName.setText(album.getName());
        }


    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_album, parent, false);

        return  new AlbumViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        AlbumViewHolder albumViewHolder = (AlbumViewHolder) holder;

        albumViewHolder.bind(position,albums.get(position));
    }

    @Override
    public int getItemCount() {
        return albums.size();
    }
}
