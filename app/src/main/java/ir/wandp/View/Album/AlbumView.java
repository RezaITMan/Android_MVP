package ir.wandp.View.Album;

import java.util.ArrayList;

import ir.wandp.Service.Model.Album;

public interface AlbumView {
    void showProgress();

    void hideProgress();

    void OnAlbumReceived(ArrayList<Album> albums);

    void onErrorReceived(String error);

}
