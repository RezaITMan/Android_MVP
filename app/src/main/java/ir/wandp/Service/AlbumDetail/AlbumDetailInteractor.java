package ir.wandp.Service.AlbumDetail;

import java.util.ArrayList;

import ir.wandp.Service.Model.AlbumDetail;

public interface AlbumDetailInteractor {
    interface OnAlbumDetailListener {
        void onError(String error);

        void onSuccess(ArrayList<AlbumDetail> albums);
    }

    void getAlbumDetail(long id, OnAlbumDetailListener listener);
}
