package ir.wandp.Service.Album;

import java.util.ArrayList;

import ir.wandp.Service.Model.Album;

public interface AlbumInteractor {
    interface OnAlbumListListener {
        void onError(String error);

        void onSuccess(ArrayList<Album> albums);
    }

    void getAlbumList(String filter, OnAlbumListListener listener);
}
