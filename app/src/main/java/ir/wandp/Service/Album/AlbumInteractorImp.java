package ir.wandp.Service.Album;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import ir.wandp.Service.Model.Album;
import ir.wandp.Service.RequestInteractor;
import ir.wandp.Service.RequestManager;

public class AlbumInteractorImp implements AlbumInteractor {
    @Override
    public void getAlbumList(String filter, final OnAlbumListListener listener) {

        RequestManager.getInstance().getJsonArrayRequest(RequestManager.AlbumUrl, "AlbumList", new RequestInteractor.onRequestListener() {
            @Override
            public void onResponse(String response) {
                Type listType = new TypeToken<ArrayList<Album>>(){}.getType();
                ArrayList<Album> albums = new Gson().fromJson(response,listType);

                listener.onSuccess(albums);
            }

            @Override
            public void onErrorResponse(String error) {
                listener.onError(error);
            }
        });
    }
}
