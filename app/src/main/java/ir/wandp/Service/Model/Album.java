package ir.wandp.Service.Model;

import com.google.gson.annotations.SerializedName;

public class Album {

    @SerializedName("Id")
    private long id;
    @SerializedName("Name")
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
