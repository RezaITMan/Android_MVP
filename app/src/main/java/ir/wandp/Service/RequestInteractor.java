package ir.wandp.Service;

public interface RequestInteractor {

    interface onRequestListener {
        void onResponse(String response);

        void onErrorResponse(String error);
    }


    void getJsonArrayRequest(String url,String tag,onRequestListener requestListener);

}
