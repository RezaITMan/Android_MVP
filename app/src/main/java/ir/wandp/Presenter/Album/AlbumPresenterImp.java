package ir.wandp.Presenter.Album;

import java.util.ArrayList;

import ir.wandp.Service.Album.AlbumInteractor;
import ir.wandp.Service.Model.Album;
import ir.wandp.View.Album.AlbumView;

public class AlbumPresenterImp implements AlbumPresenter {

    private AlbumView mAlbumView;
    private AlbumInteractor mAlbumInteractor;

    public AlbumPresenterImp(AlbumView loginView, AlbumInteractor loginInteractor) {
        this.mAlbumView = loginView;
        this.mAlbumInteractor = loginInteractor;
    }
    @Override
    public void getAlbumList(String filter) {
        mAlbumView.showProgress();
        mAlbumInteractor.getAlbumList(filter, new AlbumInteractor.OnAlbumListListener() {
            @Override
            public void onError(String error) {
                mAlbumView.hideProgress();
                mAlbumView.onErrorReceived(error);
            }

            @Override
            public void onSuccess(ArrayList<Album> albums) {
                mAlbumView.OnAlbumReceived(albums);
                mAlbumView.hideProgress();
            }
        });
    }
}
