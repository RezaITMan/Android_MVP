package ir.wandp.Presenter.AlbumDetail;

public interface AlbumDetailPresenter {
    void getAlbumDetail(long id);
}
