package ir.wandp.Presenter.AlbumDetail;

import java.util.ArrayList;

import ir.wandp.Service.AlbumDetail.AlbumDetailInteractor;
import ir.wandp.Service.Model.AlbumDetail;
import ir.wandp.View.AlbumDetail.AlbumDetailView;

public class AlbumDetailPresenterImp implements AlbumDetailPresenter {


    private AlbumDetailView mAlbumView;
    private AlbumDetailInteractor mAlbumInteractor;

    public AlbumDetailPresenterImp(AlbumDetailView albumDetailView, AlbumDetailInteractor albumDetailInteractor) {
        this.mAlbumView = albumDetailView;
        this.mAlbumInteractor = albumDetailInteractor;
    }
    @Override
    public void getAlbumDetail(long id) {
        mAlbumInteractor.getAlbumDetail(id, new AlbumDetailInteractor.OnAlbumDetailListener() {
            @Override
            public void onError(String error) {
                mAlbumView.hideProgress();
                mAlbumView.onErrorReceived(error);
            }

            @Override
            public void onSuccess(ArrayList<AlbumDetail> albums) {

                mAlbumView.OnAlbumDetailReceived(albums);
                mAlbumView.hideProgress();
            }
        });
    }
}
